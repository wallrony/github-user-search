import 'package:githubusersearch/db/api/api_github_user.dart';
import 'package:githubusersearch/db/shared_pref/shared_pref_utils.dart';
import 'package:githubusersearch/model/github_user.dart';
import 'package:githubusersearch/model/project_repository.dart';

class Facade {
  ApiGithubUser _apiGithubUser = new ApiGithubUser();
  SharedPrefUtils _prefUtils = new SharedPrefUtils();

  /// OPERATIONS WITH GITHUB API
  Future<GitHubUser> getGitHubUser(String userName) async => await
    _apiGithubUser.getGitHubUser(userName);

  Future<List<ProjectRepository>> getUserRepositories(String userName) async =>
    await _apiGithubUser.getUserRepositories(userName);

  Future<List<GitHubUser>> getFavoredUsersData() async => await
    _apiGithubUser.getFavoredUsersData();

  /// OPERATIONS WITH SHARED PREFERENCES
  Future<bool> saveAppViewMode(bool isDarkMode) async => await
    _prefUtils.saveAppViewMode(isDarkMode);

  Future<bool> getAppViewMode() async => await _prefUtils.getAppViewMode();

  Future<bool> addFavoredUser(String userName) async =>
      await _prefUtils.addFavoredUser(userName);

  Future<bool> removeFavoredUser(String userName) async =>
      await _prefUtils.removeFavoredUser(userName);

  Future<bool> isFavored(String userName) async =>
      await _prefUtils.isFavored(userName);

  Future<List<String>> getFavoredUsers() async => await
      _prefUtils.getFavoredUsers();

  Future<int> favoredUsersCount() async => await _prefUtils.favoredUsersCount();
}