class ProjectRepository {
  String _name;
  String _fullName;
  String _description;
  String _updatedAt;
  String _pushedAt;
  String _htmlUrl;
  int _stargazersCount;
  int _watchersCount;
  String _language;

  ProjectRepository(
      {String name,
        String fullName,
        String description,
        String updatedAt,
        String pushedAt,
        String htmlUrl,
        int stargazersCount,
        int watchersCount,
        String language}) {
    this._name = name;
    this._fullName = fullName;
    this._description = description;
    this._updatedAt = updatedAt;
    this._pushedAt = pushedAt;
    this._htmlUrl = htmlUrl;
    this._stargazersCount = stargazersCount;
    this._watchersCount = watchersCount;
    this._language = language;
  }

  String get name => _name;
  String get fullName => _fullName;
  String get description => _description;
  String get updatedAt => _updatedAt;
  String get pushedAt => _pushedAt;
  String get htmlUrl => _htmlUrl;
  int get stargazersCount => _stargazersCount;
  int get watchersCount => _watchersCount;
  String get language => _language;

  ProjectRepository.fromJson(Map<String, dynamic> json) {
    _name = json['name'];
    _fullName = json['full_name'];
    _description = json['description'];
    _updatedAt = json['updated_at'];
    _pushedAt = json['pushed_at'];
    _htmlUrl = json['html_url'];
    _stargazersCount = json['stargazers_count'];
    _watchersCount = json['watchers_count'];
    _language = json['language'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this._name;
    data['full_name'] = this._fullName;
    data['description'] = this._description;
    data['updated_at'] = this._updatedAt;
    data['pushed_at'] = this._pushedAt;
    data['html_url'] = this._htmlUrl;
    data['stargazers_count'] = this._stargazersCount;
    data['watchers_count'] = this._watchersCount;
    data['language'] = this._language;
    return data;
  }
}