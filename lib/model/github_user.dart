class GitHubUser {
  String _userName;
  int _id;
  String _avatarUrl;
  String _htmlUrl;
  bool _siteAdmin;
  String _name;
  String _location;
  String _bio;
  int _publicRepos;
  int _followers;
  int _following;
  String _createdAt;

  GitHubUser(
      {String userName,
        int id,
        String avatarUrl,
        String htmlUrl,
        bool siteAdmin,
        String name,
        String location,
        String bio,
        int publicRepos,
        int followers,
        int following,
        String createdAt}) {
    this._userName = userName;
    this._id = id;
    this._avatarUrl = avatarUrl;
    this._htmlUrl = htmlUrl;
    this._siteAdmin = siteAdmin;
    this._name = name;
    this._location = location;
    this._bio = bio;
    this._publicRepos = publicRepos;
    this._followers = followers;
    this._following = following;
    this._createdAt = createdAt;
  }

  String get userName => _userName;
  int get id => _id;
  String get avatarUrl => _avatarUrl;
  String get htmlUrl => _htmlUrl;
  bool get siteAdmin => _siteAdmin;
  String get name => _name;
  String get location => _location;
  String get bio => _bio;
  int get publicRepos => _publicRepos;
  int get followers => _followers;
  int get following => _following;
  String get createdAt => _createdAt;

  GitHubUser.fromJson(Map<String, dynamic> json) {
    _userName = json['login'];
    _id = json['id'];
    _avatarUrl = json['avatar_url'];
    _htmlUrl = json['html_url'];
    _siteAdmin = json['site_admin'];
    _name = json['name'];
    _location = json['location'];
    _bio = json['bio'];
    _publicRepos = json['public_repos'];
    _followers = json['followers'];
    _following = json['following'];
    _createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_name'] = this._userName;
    data['id'] = this._id;
    data['avatar_url'] = this._avatarUrl;
    data['html_url'] = this._htmlUrl;
    data['site_admin'] = this._siteAdmin;
    data['name'] = this._name;
    data['location'] = this._location;
    data['bio'] = this._bio;
    data['public_repos'] = this._publicRepos;
    data['followers'] = this._followers;
    data['following'] = this._following;
    data['created_at'] = this._createdAt;
    return data;
  }

  @override
  String toString() {
    // TODO: implement toString
    return this.toJson().toString();
  }
}