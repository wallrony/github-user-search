// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'search_page_model.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$SearchPageModel on SearchPageModelBase, Store {
  final _$_userAtom = Atom(name: 'SearchPageModelBase._user');

  @override
  GitHubUser get _user {
    _$_userAtom.context.enforceReadPolicy(_$_userAtom);
    _$_userAtom.reportObserved();
    return super._user;
  }

  @override
  set _user(GitHubUser value) {
    _$_userAtom.context.conditionallyRunInAction(() {
      super._user = value;
      _$_userAtom.reportChanged();
    }, _$_userAtom, name: '${_$_userAtom.name}_set');
  }

  final _$_errorAtom = Atom(name: 'SearchPageModelBase._error');

  @override
  String get _error {
    _$_errorAtom.context.enforceReadPolicy(_$_errorAtom);
    _$_errorAtom.reportObserved();
    return super._error;
  }

  @override
  set _error(String value) {
    _$_errorAtom.context.conditionallyRunInAction(() {
      super._error = value;
      _$_errorAtom.reportChanged();
    }, _$_errorAtom, name: '${_$_errorAtom.name}_set');
  }

  final _$_userRepositoriesAtom =
      Atom(name: 'SearchPageModelBase._userRepositories');

  @override
  List<ProjectRepository> get _userRepositories {
    _$_userRepositoriesAtom.context.enforceReadPolicy(_$_userRepositoriesAtom);
    _$_userRepositoriesAtom.reportObserved();
    return super._userRepositories;
  }

  @override
  set _userRepositories(List<ProjectRepository> value) {
    _$_userRepositoriesAtom.context.conditionallyRunInAction(() {
      super._userRepositories = value;
      _$_userRepositoriesAtom.reportChanged();
    }, _$_userRepositoriesAtom, name: '${_$_userRepositoriesAtom.name}_set');
  }

  final _$_favoredUserAtom = Atom(name: 'SearchPageModelBase._favoredUser');

  @override
  bool get _favoredUser {
    _$_favoredUserAtom.context.enforceReadPolicy(_$_favoredUserAtom);
    _$_favoredUserAtom.reportObserved();
    return super._favoredUser;
  }

  @override
  set _favoredUser(bool value) {
    _$_favoredUserAtom.context.conditionallyRunInAction(() {
      super._favoredUser = value;
      _$_favoredUserAtom.reportChanged();
    }, _$_favoredUserAtom, name: '${_$_favoredUserAtom.name}_set');
  }

  final _$fetchUserDataAsyncAction = AsyncAction('fetchUserData');

  @override
  Future fetchUserData(String userName) {
    return _$fetchUserDataAsyncAction.run(() => super.fetchUserData(userName));
  }

  final _$fetchUserRepositoriesAsyncAction =
      AsyncAction('fetchUserRepositories');

  @override
  Future fetchUserRepositories(String userName) {
    return _$fetchUserRepositoriesAsyncAction
        .run(() => super.fetchUserRepositories(userName));
  }

  final _$SearchPageModelBaseActionController =
      ActionController(name: 'SearchPageModelBase');

  @override
  dynamic setFavoredUser(dynamic value) {
    final _$actionInfo = _$SearchPageModelBaseActionController.startAction();
    try {
      return super.setFavoredUser(value);
    } finally {
      _$SearchPageModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic _treatError(String error, bool userFetch) {
    final _$actionInfo = _$SearchPageModelBaseActionController.startAction();
    try {
      return super._treatError(error, userFetch);
    } finally {
      _$SearchPageModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    final string = '';
    return '{$string}';
  }
}
