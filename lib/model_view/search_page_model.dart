import 'package:githubusersearch/facade/facade.dart';
import 'package:githubusersearch/model/github_user.dart';
import 'package:githubusersearch/model/project_repository.dart';
import 'package:mobx/mobx.dart';

part 'search_page_model.g.dart';

class SearchPageModel = SearchPageModelBase with _$SearchPageModel;

abstract class SearchPageModelBase with Store {
  final Facade _facade = new Facade();

  @observable
  GitHubUser _user;
  GitHubUser get user => _user;

  @observable
  String _error;
  String get error => _error;

  @observable
  List<ProjectRepository> _userRepositories;
  List<ProjectRepository> userRepositories;

  @observable
  bool _favoredUser = false;
  bool get favoredUser => _favoredUser;
  @action setFavoredUser(value) => _favoredUser = value;

  @action
  fetchUserData(String userName) async {
    GitHubUser user = await _facade.getGitHubUser(userName).catchError((error) {
      _treatError(error, true);

      if(_user != null) {
        _user = null;
      }

      throw(error);
    });

    print(user.toString());

    if(user != null) {
      this._user = user;
    }
  }

  @action
  fetchUserRepositories(String userName) async {
    List<ProjectRepository> repositories = await _facade.getUserRepositories(
      userName
    ).catchError((error) {
      _treatError(error, false);

      return null;
    });

    if(repositories != null) {
      this._userRepositories = repositories;
    }
  }

  @action
  _treatError(String error, bool userFetch) {
    if(userFetch) {
      if(error.contains("username-incorrect-or-not-existent")) {
        this._error = "username-incorrect-or-not-existent";
      }
      else if(error.contains("without-network")) {
        this._error = "without-network";
      }
    }
    else {
      if(error.contains("user-have-anyone-repository")) {
        this._error = "user-have-anyone-repository";
      }
      else if(error.contains("without-network")) {
        this._error = "without-network";
      }
    }
  }
}
