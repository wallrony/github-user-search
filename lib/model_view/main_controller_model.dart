import 'package:mobx/mobx.dart';

part 'main_controller_model.g.dart';

class MainControllerModel = MainControllerModelBase with _$MainControllerModel;

abstract class MainControllerModelBase with Store {
  @observable
  bool _darkModeActivated = false;
  bool get darkModeActivated => _darkModeActivated;

  @action
  changeDarkModeState(bool state) {
    this._darkModeActivated = state;
  }
}
