// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_page_model.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$HomePageModel on HomePageModelBase, Store {
  final _$_favoredUsersAtom = Atom(name: 'HomePageModelBase._favoredUsers');

  @override
  List<GitHubUser> get _favoredUsers {
    _$_favoredUsersAtom.context.enforceReadPolicy(_$_favoredUsersAtom);
    _$_favoredUsersAtom.reportObserved();
    return super._favoredUsers;
  }

  @override
  set _favoredUsers(List<GitHubUser> value) {
    _$_favoredUsersAtom.context.conditionallyRunInAction(() {
      super._favoredUsers = value;
      _$_favoredUsersAtom.reportChanged();
    }, _$_favoredUsersAtom, name: '${_$_favoredUsersAtom.name}_set');
  }

  final _$fetchFavoredUsersAsyncAction = AsyncAction('fetchFavoredUsers');

  @override
  Future fetchFavoredUsers() {
    return _$fetchFavoredUsersAsyncAction.run(() => super.fetchFavoredUsers());
  }

  @override
  String toString() {
    final string = '';
    return '{$string}';
  }
}
