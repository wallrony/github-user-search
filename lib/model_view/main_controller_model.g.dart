// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'main_controller_model.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$MainControllerModel on MainControllerModelBase, Store {
  final _$_darkModeActivatedAtom =
      Atom(name: 'MainControllerModelBase._darkModeActivated');

  @override
  bool get _darkModeActivated {
    _$_darkModeActivatedAtom.context
        .enforceReadPolicy(_$_darkModeActivatedAtom);
    _$_darkModeActivatedAtom.reportObserved();
    return super._darkModeActivated;
  }

  @override
  set _darkModeActivated(bool value) {
    _$_darkModeActivatedAtom.context.conditionallyRunInAction(() {
      super._darkModeActivated = value;
      _$_darkModeActivatedAtom.reportChanged();
    }, _$_darkModeActivatedAtom, name: '${_$_darkModeActivatedAtom.name}_set');
  }

  final _$MainControllerModelBaseActionController =
      ActionController(name: 'MainControllerModelBase');

  @override
  dynamic changeDarkModeState(bool state) {
    final _$actionInfo =
        _$MainControllerModelBaseActionController.startAction();
    try {
      return super.changeDarkModeState(state);
    } finally {
      _$MainControllerModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    final string = '';
    return '{$string}';
  }
}
