import 'package:githubusersearch/facade/facade.dart';
import 'package:githubusersearch/model/github_user.dart';
import 'package:githubusersearch/utils/utils.dart';
import 'package:mobx/mobx.dart';

part 'home_page_model.g.dart';

class HomePageModel = HomePageModelBase with _$HomePageModel;

abstract class HomePageModelBase with Store {
  @observable
  List<GitHubUser> _favoredUsers;
  List<GitHubUser> get favoredUsers => _favoredUsers;

  @action
  fetchFavoredUsers() async {
    print("Fetching list...");
    _favoredUsers = null;
    List<GitHubUser> users = await Facade().getFavoredUsersData();
    _favoredUsers = users;
  }
}
