import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:githubusersearch/pages/home_page.dart';
import 'package:githubusersearch/pages/search_page.dart';
import 'package:githubusersearch/widgets/custom_alert_dialog.dart';
import 'package:githubusersearch/widgets/custom_simple_button.dart';
import 'package:githubusersearch/widgets/custom_text.dart';

class Utils {
  /// The method below returns true if is connected and false cause not.
  static Future<bool> getNetworkStatus() async {
    ConnectivityResult result = await Connectivity().checkConnectivity();

    if (result == ConnectivityResult.mobile ||
        result == ConnectivityResult.wifi) {
      return true;
    } else {
      return false;
    }
  }

  static showLoadingDialog(BuildContext context, bool darkModeActivated) {
    showDialog(
      context: context,
      barrierDismissible: false,
      child: CustomAlertDialog(
        isInDarkMode: darkModeActivated,
        loading: true,
      ),
    );
  }

  static showAlertDialog(
      BuildContext context, String title, String content, List<Widget> actions,
      {bool darkModeActivated}) {
    if (actions == null) {
      actions = [
        CustomSimpleButton(
          isInDarkMode: darkModeActivated ?? false,
          text: "Ok",
          onPressed: () => closeDialog(context),
        ),
      ];
    }

    showDialog(
      context: context,
      barrierDismissible: false,
      child: CustomAlertDialog(
        title: title,
        content: content,
        isInDarkMode: darkModeActivated ?? false,
        actions: actions,
      ),
    );
  }

  static closeDialog(BuildContext context) {
    Navigator.of(context).pop();
  }

  static createPageRoute(int index) {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) =>
          index == 0 ? HomePage() : SearchPage(),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var tween =
          Tween(
              begin: Offset(1.0, 0.0),
              end: Offset.zero
          ).chain(CurveTween(curve: Curves.linearToEaseOut));

        var offsetAnimation = animation.drive(tween);

        return SlideTransition(
          position: offsetAnimation,
          child: child,
        );
      },
    );
  }

  static makeAppBar(bool isInDarkMode) {
    return AppBar(
      backgroundColor: Colors.indigoAccent,
      title: CustomText(
        text: "GitHub User Search",
        fontSize: 24,
        isInDarkMode: isInDarkMode,
        fontFamily: "Raleway",
        isBold: true,
        color: Colors.white
      ),
      leading: null,
      centerTitle: true,
      bottomOpacity: 0.5,
    );
  }

  static appLinearGradient({ bool invertColors = false }) {
    return LinearGradient(
      begin: invertColors ? Alignment.topLeft : Alignment.topRight,
      end: invertColors ? Alignment.bottomRight : Alignment.bottomLeft,
      colors: [
        Colors.indigoAccent,
        Colors.indigo
      ],
    );
  }
}
