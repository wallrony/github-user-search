import 'package:flutter/material.dart';

void push(BuildContext context, PageRouteBuilder route,
    {bool replace = false, Function whenPop}) {
  if (replace) {
    Navigator.pushReplacement(context, route);
  }
  else {
    Navigator.push(context, route).then((value) {
      whenPop();
    });
  }
}