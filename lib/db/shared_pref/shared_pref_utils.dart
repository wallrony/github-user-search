import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefUtils {
  Map<String, String> _sharedPrefKeys = {
    'dark_mode': 'githubUserSearch@darkModeActivated',
    'favored_users': 'githubUserSearch@favoresUsers',
  };

  Future<bool> saveAppViewMode(isDarkMode) async {
    SharedPreferences pref = await getSharedPreferencesInstance();

    bool result = await pref.setBool(_sharedPrefKeys['dark_mode'], isDarkMode);

    return result;
  }

  Future<bool> getAppViewMode() async {
    SharedPreferences pref = await getSharedPreferencesInstance();

    bool result = pref.getBool(_sharedPrefKeys['dark_mode']);

    return result;
  }

  Future<bool> addFavoredUser(String userName) async {
    SharedPreferences pref = await getSharedPreferencesInstance();

    List<String> favoredUsers = await getFavoredUsers() ?? new List();

    favoredUsers.add(userName);

    String favoredUsersInStr = "";

    for (int i = 0; i < favoredUsers.length; i++) {
      if (favoredUsers[i].isEmpty) continue;

      favoredUsersInStr += favoredUsers[i];
      favoredUsersInStr += i == (favoredUsers.length - 1) ? '' : '*;*';
    }

    bool result = await pref.setString(
        _sharedPrefKeys['favored_users'], favoredUsersInStr);

    return result;
  }

  Future<bool> removeFavoredUser(String userName) async {
    SharedPreferences pref = await getSharedPreferencesInstance();

    List<String> favoredUsers = await getFavoredUsers();

    while (favoredUsers.contains(userName)) favoredUsers.remove(userName);

    String favoredUsersInStr = "";

    for (int i = 0; i < favoredUsers.length; i++) {
      if (favoredUsers[i].isEmpty) continue;

      favoredUsersInStr += favoredUsers[i];
      favoredUsersInStr += i == (favoredUsers.length - 1) ? '' : '*;*';
    }

    bool result = await pref.setString(
        _sharedPrefKeys['favored_users'], favoredUsersInStr);

    return result;
  }

  Future<List<String>> getFavoredUsers() async {
    SharedPreferences pref = await getSharedPreferencesInstance();

    String result = pref.getString(_sharedPrefKeys['favored_users']);

    if (result == null) return new List<String>();

    print(result);

    List<String> favoredUsers = result.split('*;*');

    return favoredUsers;
  }

  Future<bool> isFavored(String userName) async {
    List<String> favoredUsers = await getFavoredUsers();

    if (favoredUsers.contains(userName)) {
      return true;
    }

    return false;
  }

  Future<SharedPreferences> getSharedPreferencesInstance() async =>
      await SharedPreferences.getInstance();

  Future<int> favoredUsersCount() async =>
      (await this.getFavoredUsers()).length;
}
