import 'dart:convert';

import 'package:githubusersearch/facade/facade.dart';
import 'package:githubusersearch/model/github_user.dart';
import 'package:githubusersearch/model/project_repository.dart';
import 'package:githubusersearch/utils/utils.dart';
import 'package:http/http.dart' as http;

class ApiGithubUser {
  final String _apiUrl = "https://api.github.com/users";
  final Map<String, String> _customHeaders = {
    "Content-Type": "application/json"
  };

  Future<GitHubUser> getGitHubUser(String userName) async {
    bool isConnected = await Utils.getNetworkStatus();

    if(!isConnected) {
      throw("without-connection");
    }

    var response = await http.get('$_apiUrl/$userName');
    var jsonResponse = json.decode(utf8.decode(response.bodyBytes));

    if(jsonResponse['message'].toString().compareTo("Not Found") == 0) {
      throw("username-incorrect-or-not-existent");
    }

    return GitHubUser.fromJson(jsonResponse);
  }

  Future<List<ProjectRepository>> getUserRepositories(String userName) async {
    bool isConnected = await Utils.getNetworkStatus();

    if(!isConnected) {
      throw("without-connection");
    }

    var response = await http.get(
      '$_apiUrl/$userName/repos',
      headers: _customHeaders
    );

    List<ProjectRepository> repositoryList;

    var repositories = json.decode(utf8.decode(response.bodyBytes));

    if(Map.from(repositories).isNotEmpty) {
      for(Map<String, dynamic> repository in repositories) {
        repositoryList.add(ProjectRepository.fromJson(repository));
      }
    }
    else {
      throw("user-have-anyone-repository");
    }

    return repositoryList;
  }

  Future<List<GitHubUser>> getFavoredUsersData() async {
    if(!(await Utils.getNetworkStatus())) {
      throw('withoutConnection');
    }

    List<String> listOfUsers = await Facade().getFavoredUsers() ?? List();
    List<GitHubUser> favoredUsers = new List();

    for(String userName in listOfUsers) {
      print("UserName: $userName");
      favoredUsers.add(await this.getGitHubUser(userName));
    }

    print(favoredUsers);

    return favoredUsers;
  }
}