import 'package:flutter/material.dart';
import 'package:githubusersearch/pages/splash_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'GitHub User Search',
      home: SplashPage(),
      debugShowCheckedModeBanner: false,
    );
  }
}