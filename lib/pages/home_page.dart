import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:githubusersearch/facade/facade.dart';
import 'package:githubusersearch/model_view/home_page_model.dart';
import 'package:githubusersearch/utils/navigator_functions.dart';
import 'package:githubusersearch/utils/utils.dart';
import 'package:githubusersearch/widgets/custom_button.dart';
import 'package:githubusersearch/widgets/custom_progress_indicator.dart';
import 'package:githubusersearch/widgets/custom_text.dart';
import 'package:githubusersearch/widgets/custom_user_item.dart';

class HomePage extends StatefulWidget {
  final Function changeAppViewMode;
  final Function changePage;
  final bool isInDarkMode;

  HomePage(
      {this.changeAppViewMode, this.changePage, this.isInDarkMode = false});

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final HomePageModel _homeModel = new HomePageModel();

  @override
  void initState() {
    super.initState();

    _homeModel.fetchFavoredUsers();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Utils.makeAppBar(widget.isInDarkMode),
      body: _makeBody(),
    );
  }

  Widget _makeBody() {
    return Container(
      height: double.maxFinite,
      width: double.maxFinite,
      decoration: BoxDecoration(
        gradient: Utils.appLinearGradient(),
      ),
      child: Padding(
        padding: EdgeInsets.all(16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.all(16),
              child: CustomText(
                text: "Seja bem vindo ao pesquisador de usuários do GitHub!",
                fontSize: 20,
                textAlign: TextAlign.center,
                fontFamily: "Raleway",
                isBold: true,
                color: Colors.white,
              ),
            ),
            CustomButton(
              text: "Pesquisar agora mesmo",
              fontSize: 18,
              isInDarkMode: widget.isInDarkMode,
              onPressed: () => push(context, Utils.createPageRoute(1),
                  whenPop: _verifyListUpdate),
              fontFamily: "Raleway",
              isBold: true,
              padding: 10,
            ),
            Container(
                margin: EdgeInsets.symmetric(vertical: 16),
                child: Divider(
                  height: 1,
                  thickness: 2,
                  color: Colors.white,
                )),
            Observer(builder: (_) {
              return _homeModel.favoredUsers == null
                  ? Container(
                      child: Wrap(
                          direction: Axis.vertical,
                          alignment: WrapAlignment.center,
                          crossAxisAlignment: WrapCrossAlignment.center,
                          children: [
                            CustomProgressIndicator(),
                            Container(
                                margin: EdgeInsets.only(top: 8),
                                child: CustomText(
                                  text: "Carregando usuários favoritados...",
                                  color: Colors.white,
                                ))
                          ]),
                    )
                  : _homeModel.favoredUsers.isEmpty
                      ? Container(
                          padding:
                              EdgeInsets.symmetric(horizontal: 12, vertical: 6),
                          width: double.maxFinite,
                          decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.95),
                            borderRadius: BorderRadius.circular(4),
                          ),
                          child: CustomText(
                            text: "Você ainda não favoritou nenhum usuário.",
                            fontSize: 26,
                            textAlign: TextAlign.center,
                          ),
                        )
                      : _makeFavoredUsersList();
            })
          ],
        ),
      ),
    );
  }

  _makeFavoredUsersList() {
    return Flexible(
        child: Container(
            height: 300,
            padding: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
            width: double.maxFinite,
            decoration: BoxDecoration(
              color: Colors.white.withOpacity(0.95),
              borderRadius: BorderRadius.circular(4),
            ),
            child: Observer(builder: (_) {
              return AnimatedList(
                  initialItemCount: _homeModel.favoredUsers.length,
                  itemBuilder: (context, index, animation) {
                    return SlideTransition(
                        position: animation.drive(Tween(
                                begin: Offset(1.0, 0.0), end: Offset.zero)
                            .chain(CurveTween(curve: Curves.linearToEaseOut))),
                        child: _favoredListItem(context, index));
                  });
            })));
  }

  _favoredListItem(BuildContext context, int index) {
    return Container(
        margin: EdgeInsets.only(bottom: 5, left: 5),
        child: CustomUserItem(
          user: _homeModel.favoredUsers[index],
          isInDarkMode: widget.isInDarkMode,
          removeUserFromList: _removeUserFromFavoredList,
        ));
  }

  _removeUserFromFavoredList(String userName) {}

  _updateList() => _homeModel.fetchFavoredUsers();

  _verifyListUpdate() async {
    int count = await Facade().favoredUsersCount();

    print("Verifying count...");

    print("$count - ${_homeModel.favoredUsers.length}");

    if (count != _homeModel.favoredUsers.length) _updateList();
  }
}
