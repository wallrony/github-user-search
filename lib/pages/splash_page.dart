import 'dart:async';

import 'package:flutter/material.dart';
import 'package:githubusersearch/utils/navigator_functions.dart';
import 'package:githubusersearch/utils/utils.dart';
import 'package:githubusersearch/widgets/custom_text.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Timer(
      Duration(seconds: 3), () {
        push(context, Utils.createPageRoute(0), replace: true);
      }
    );

    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: Utils.appLinearGradient()
        ),
        alignment: Alignment.center,
        width: double.maxFinite,
        height: double.maxFinite,
        child: Wrap(
          direction: Axis.horizontal,
          crossAxisAlignment: WrapCrossAlignment.center,
          children: [
            Icon(
              Icons.search, size: 140,
              color: Colors.white,
            ),
            CustomText(
              text: "GitHub\nUser\nSearcher",
              fontFamily: "Raleway",
              color: Colors.white,
              isBold: true,
              fontSize: 30,
              textAlign: TextAlign.left,
            ),
          ],
        ),
      ),
    );
  }
}

