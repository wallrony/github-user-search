import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:githubusersearch/facade/facade.dart';
import 'package:githubusersearch/model/github_user.dart';
import 'package:githubusersearch/model_view/search_page_model.dart';
import 'package:githubusersearch/utils/utils.dart';
import 'package:githubusersearch/widgets/custom_button_icon.dart';
import 'package:githubusersearch/widgets/custom_text.dart';
import 'package:githubusersearch/widgets/custom_text_field.dart';
import 'package:statusbar/statusbar.dart';

class SearchPage extends StatefulWidget {
  final bool isInDarkMode;

  SearchPage({this.isInDarkMode = false});

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage>
    with SingleTickerProviderStateMixin {
  final SearchPageModel _searchModel = new SearchPageModel();

  final TextEditingController _searchController = new TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    StatusBar.color(Colors.indigo);

    return Scaffold(
      body: SafeArea(child: _makeBody()),
    );
  }

  _makeTopBar() {
    return Row(children: [
      Flexible(
          flex: 4,
          child: Container(
            width: MediaQuery.of(context).size.width * 0.6,
            padding: EdgeInsets.all(5),
            margin: EdgeInsets.only(
              left: 16,
            ),
            child: CustomTextField(
              this._searchController,
              onEditingComplete: () => _makeSearch(),
              hint: "Ex.: tiopatinhas",
              fontSize: 20,
            ),
          )),
      Flexible(
          flex: 3,
          child: CustomButtonIcon(
            isInDarkMode: widget.isInDarkMode,
            text: "Pesquisar",
            onPressed: () => _makeSearch(),
            icon: Icons.search,
            color: Colors.white,
          ))
    ]);
  }

  _makeBody() {
    return Container(
        height: double.maxFinite,
        width: double.maxFinite,
        decoration: BoxDecoration(
          gradient: Utils.appLinearGradient(invertColors: true),
        ),
        child: Observer(builder: (_) {
          return Column(children: [
            _makeTopBar(),
            Flexible(
                flex: 1,
                child: Container(
                    child: _searchModel.user != null
                        ? _makeUserItem(_searchModel.user)
                        : _showMessage()))
          ]);
        }));
  }

  _makeUserItem(GitHubUser user) {
    return GestureDetector(
      child: Center(
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white.withOpacity(0.95),
            borderRadius: BorderRadius.circular(4),
          ),
          margin: EdgeInsets.all(12),
          padding: EdgeInsets.all(16),
          child: SingleChildScrollView(
            child: Observer(builder: (_) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                  _makeHeaderItem(user),
                  CustomText(
                      text: user.bio != null
                          ? "Bio: ${user.bio}"
                          : "Esse perfil não inseriu uma descrição sobre si.",
                      textAlign:
                          user.bio != null ? TextAlign.left : TextAlign.center,
                      fontSize: 18),
                  Container(
                      margin: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                      child: Divider(
                        height: 1,
                        thickness: 2,
                        color: Colors.black54,
                      )),
                  Wrap(
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: [
                      Icon(Icons.arrow_forward_ios, color: Colors.indigo),
                      CustomText(
                        text: "Seguidores: ${user.followers}",
                        textAlign: TextAlign.left,
                        fontSize: 20,
                      ),
                    ],
                  ),
                  SizedBox(height: 18),
                  Container(
                    width: double.maxFinite,
                    alignment: Alignment.center,
                    child: RaisedButton(
                      color: Colors.indigoAccent,
                      focusColor: Colors.blue,
                      onPressed: () => {},
                      child: Container(
                          width: double.maxFinite,
                          alignment: Alignment.center,
                          child: Wrap(
                              crossAxisAlignment: WrapCrossAlignment.center,
                              children: [
                                Container(
                                    margin: EdgeInsets.only(right: 10),
                                    child: Icon(
                                      Icons.list,
                                      size: 40,
                                      color: Colors.white70,
                                    )),
                                CustomText(
                                  text: "Ver Repositórios Públicos (${user.publicRepos})",
                                  fontSize: 18,
                                  color: Colors.white,
                                ),
                              ])),
                    ),
                  )
                ]);
          }),
        ),
      )),
      onTap: _showDetailUserPage,
    );
  }

  _makeHeaderItem(GitHubUser user) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Flexible(
            flex: 1,
            child: CircleAvatar(
              backgroundImage:
              NetworkImage(_searchModel.user.avatarUrl),
            )
        ),
        Flexible(
            flex: 5,
            child: Container(
              margin: EdgeInsets.only(left: 10),
              child: CustomText(
                text: user.name != null ? "${user.name}@${user.userName}" : "@${user.userName}",
                fontSize: 20,
                color: Colors.indigo,
              ),
            )
        ),
        Flexible(
            flex: 2,
            child: RaisedButton(
              color: Colors.transparent,
              focusColor: Colors.blue,
              disabledElevation: 0,
              elevation: 0,
              focusElevation: 1,
              highlightElevation: 1,
              hoverElevation: 1,
              padding: EdgeInsets.all(4),
              onPressed: () => _changeFavoredUser(),
              child: Wrap(
                  direction: Axis.vertical,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    Icon(
                      _searchModel.favoredUser
                          ? Icons.star
                          : Icons.star_border,
                      size: 40,
                      color: _searchModel.favoredUser
                          ? Colors.amber
                          : Colors.grey,
                    ),
                    CustomText(
                      text: _searchModel.favoredUser
                          ? "Desfavoritar"
                          : "Favoritar",
                    ),
                  ]),
            )
        ),
      ],
    );
  }

  _changeFavoredUser() async {
    Utils.showLoadingDialog(context, widget.isInDarkMode);

    bool result = false;

    if (_searchModel.favoredUser) {
      result = await Facade().removeFavoredUser(_searchModel.user.userName);
    } else {
      result = await Facade().addFavoredUser(_searchModel.user.userName);
    }

    if (result) {
      _searchModel.setFavoredUser(!_searchModel.favoredUser);
    }

    Utils.closeDialog(context);
  }

  _showMessage() {
    return Observer(builder: (_) {
      return Padding(
        padding: EdgeInsets.all(16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              _searchModel.error == null
                  ? _searchModel.user == null ? Icons.watch_later : null
                  : Icons.warning,
              size: 50,
              color: Colors.white,
            ),
            CustomText(
              fontSize: 26,
              textAlign: TextAlign.center,
              fontFamily: "Raleway",
              isBold: true,
              color: Colors.white,
              text: _searchModel.error == null
                  ? _searchModel.user == null
                      ? "Esperando você digitar o nome do usuário..."
                      : ""
                  : _searchModel.error.compareTo("without-network") == 0
                      ? "Você precisa estar conectado à internet para poder pesquisar."
                      : _searchModel.error.compareTo(
                                  "username-incorrect-or-not-existent") ==
                              0
                          ? "Usuário não encontrado!"
                          : "Um erro desconhecido aconteceu. Por favor, teste mais tarde!",
            ),
          ],
        ),
      );
    });
  }

  _showDetailUserPage() {
    //Navigator.of(context).popAndPushNamed("/userDetail");
  }

  _makeSearch() async {
    if (_searchController.text.isEmpty) {
      Utils.showAlertDialog(context, "Ops!",
          "Você precisa digitar o nome de algum usuário!", null);
    } else {
      Utils.showLoadingDialog(context, widget.isInDarkMode);

      _searchModel.setFavoredUser(false);

      try {
        await _searchModel.fetchUserData(_searchController.text);

        if (await Facade().isFavored(_searchModel.user.userName)) {
          _searchModel.setFavoredUser(true);
        } else {
          _searchModel.setFavoredUser(false);
        }
      } catch(error) { } finally {
        Utils.closeDialog(context);
      }
    }
  }
}
