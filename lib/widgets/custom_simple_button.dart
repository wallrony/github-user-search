import 'package:flutter/material.dart';
import 'package:githubusersearch/widgets/custom_text.dart';

class CustomSimpleButton extends StatelessWidget {
  final String text;
  final bool isInDarkMode;
  final Function onPressed;
  final bool styledButton;

  CustomSimpleButton({
    this.text,
    this.isInDarkMode,
    this.onPressed,
    this.styledButton = true
  });

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: onPressed,
      color: styledButton ? Colors.indigoAccent : Colors.transparent,
      padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
      child: CustomText(
        text: text,
        color: styledButton ? Colors.white : Colors.black,
        isInDarkMode: isInDarkMode,
        fontSize: 20,
      ),
    );
  }
}

