import 'package:flutter/material.dart';
import 'package:githubusersearch/widgets/custom_progress_indicator.dart';
import 'package:githubusersearch/widgets/custom_text.dart';

class CustomAlertDialog extends StatelessWidget {
  final String title;
  final String content;
  final bool isInDarkMode;
  final List<Widget> actions;
  final bool loading;

  CustomAlertDialog({
    this.title,
    this.content,
    this.isInDarkMode = false,
    this.actions,
    this.loading = false,
  });

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      elevation: 0,
      backgroundColor: loading ?
        Colors.white.withOpacity(0.0) : Colors.white,
      title: !loading
        ? CustomText(
          text: title,
          fontFamily: 'Raleway',
          isBold: true,
          fontSize: 28,
          color: isInDarkMode
            ? Colors.white : Colors.black,
        ) : Container(),
      content: !loading
        ? CustomText(
          text: content,
          fontFamily: 'Baloo',
          fontSize: 18,
          color: isInDarkMode
            ? Colors.white : Colors.black,
        )
        :
        Container(
          height: 100,
          width: 100,
          decoration: BoxDecoration(
            //color: Colors.white,
            borderRadius: BorderRadius.circular(20),
            boxShadow: [],
            border: Border.all(
              color: Colors.transparent,
              width: 0,
            ),
          ),
          child: CustomProgressIndicator(
            isInDarkMode: isInDarkMode,
          )
        ),
      actions: actions,
    );
  }
}

