import 'package:flutter/material.dart';
import 'package:githubusersearch/facade/facade.dart';
import 'package:githubusersearch/model/github_user.dart';
import 'package:githubusersearch/utils/utils.dart';

import 'custom_simple_button.dart';
import 'custom_text.dart';

class CustomUserItem extends StatelessWidget {
  final GitHubUser user;
  final bool isInDarkMode;
  final Function removeUserFromList;

  CustomUserItem(
      {this.user, this.isInDarkMode = false, this.removeUserFromList});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          CircleAvatar(
            backgroundImage: NetworkImage(user.avatarUrl ?? ""),
          ),
          Container(
            margin: EdgeInsets.only(left: 10),
            child: CustomText(
              text: user.userName != null ? "@${user.userName}" : "",
              isInDarkMode: isInDarkMode,
              fontSize: 24,
              fontFamily: "Baloo",
            ),
          ),
          RaisedButton(
            color: Colors.transparent,
            focusColor: Colors.blue,
            disabledElevation: 0,
            elevation: 0,
            focusElevation: 1,
            highlightElevation: 1,
            hoverElevation: 1,
            onPressed: () => _removeOfFavoredList(context),
            child: Wrap(
                direction: Axis.vertical,
                crossAxisAlignment: WrapCrossAlignment.center,
                children: [
                  Icon(Icons.star, size: 40, color: Colors.amber),
                  CustomText(text: "Remover"),
                ]),
          ),
        ],
      ),
    );
  }

  _removeOfFavoredList(BuildContext context) {
    Utils.showAlertDialog(
        context,
        "Desfavoritar usuário",
        "Você tem certeza que deseja remover este usuário da lista de favoritos?",
        [
          CustomSimpleButton(
            styledButton: false,
            isInDarkMode: isInDarkMode ?? false,
            text: "Não",
            onPressed: () => Utils.closeDialog(context),
          ),
          CustomSimpleButton(
            styledButton: true,
            isInDarkMode: isInDarkMode ?? false,
            text: "Sim",
            onPressed: () async {
              var result = await Facade().removeFavoredUser(user.userName);

              if (result) {
                removeUserFromList(user.userName);
                Utils.closeDialog(context);
                Utils.showLoadingDialog(context, false);
              }
            },
          )
        ]);
  }
}
