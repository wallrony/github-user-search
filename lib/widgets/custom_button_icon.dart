import 'package:flutter/material.dart';
import 'package:githubusersearch/widgets/custom_text.dart';

class CustomButtonIcon extends StatelessWidget {
  final String text;
  final bool isInDarkMode;
  final Function onPressed;
  final IconData icon;
  final Color color;

  CustomButtonIcon(
      {this.text, this.isInDarkMode, this.onPressed, this.icon, this.color});

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: onPressed,
      color: Colors.transparent,
      padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
      child: Row(
        children: [
          Container(
              margin: EdgeInsets.only(
                left: 4,
              ),
              child: Icon(icon,
                  size: 30, color: color != null ? color : Colors.white)),
          Container(
              margin: EdgeInsets.only(left: 8, right: 12),
              child: CustomText(
                text: text,
                color: color != null
                    ? color
                    : isInDarkMode ? Colors.white : Colors.black,
                isInDarkMode: isInDarkMode,
                fontSize: 20,
              )),
        ],
      ),
    );
    ;
  }
}
