import 'package:flutter/material.dart';

class CustomProgressIndicator extends StatelessWidget {
  final bool isInDarkMode;

  CustomProgressIndicator({ this.isInDarkMode = false });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(
          Colors.white
        ),
      )
    );
  }
}
