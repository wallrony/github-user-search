import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  final TextEditingController controller;
  final TextInputType inputType;
  final String hint;
  final Function onTap;
  final double fontSize;
  final Function onEditingComplete;

  CustomTextField(
    this.controller,
    {
      this.inputType,
      this.hint,
      this.onTap,
      this.fontSize,
      this.onEditingComplete
    }
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.white70,
          width: 2,
          style: BorderStyle.solid
        ),
        borderRadius: BorderRadius.circular(6)
      ),
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: TextFormField(
        onTap: onTap,
        autofocus: false,
        controller: controller,
        keyboardType: inputType ?? TextInputType.text,
        cursorColor: Colors.white,
        onEditingComplete: onEditingComplete,
        decoration: InputDecoration.collapsed(
          hintText: hint,
          filled: true,
          hintStyle: TextStyle(
            color: Colors.white54
          ),
        ),
        cursorRadius: Radius.circular(5),
        style: TextStyle(
          fontFamily: 'Baloo',
          fontSize: fontSize ?? 16,
          color: Colors.white,
        ),
      )
    );
  }
}

