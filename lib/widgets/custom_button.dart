import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:githubusersearch/widgets/custom_text.dart';

class CustomButton extends StatelessWidget {
  final String text;
  final double fontSize;
  final bool isInDarkMode;
  final String fontFamily;
  final Function onPressed;
  final bool isBold;
  final double padding;

  CustomButton({
    this.text,
    this.fontSize,
    this.isInDarkMode = false,
    this.fontFamily,
    this.onPressed,
    this.isBold,
    this.padding
  });

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: onPressed,
      color: Colors.white,
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 7.5 + padding != null ? padding : 0,
          vertical: 5 + padding != null ? padding : 0
        ),
        child: CustomText(
          text: text,
          fontSize: fontSize,
          color: isInDarkMode ? Colors.white : Colors.black,
          isInDarkMode: isInDarkMode,
          fontFamily: fontFamily,
          isBold: isBold
        ),
      )
    );
  }
}

