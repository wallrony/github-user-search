import 'package:flutter/material.dart';

class CustomText extends StatelessWidget {
  final String text;
  final double fontSize;
  final Color color;
  final bool isBold;
  final bool isInDarkMode;
  final String fontFamily;
  final TextAlign textAlign;

  CustomText({
    this.text,
    this.fontSize,
    this.color,
    this.isBold = false,
    this.isInDarkMode = false,
    this.fontFamily,
    this.textAlign
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
        text,
        textAlign: textAlign,
        style: TextStyle(
          fontSize: fontSize ?? 16,
          fontWeight: isBold ? FontWeight.bold : FontWeight.normal,
          color: color == null
            ? isInDarkMode
              ? Colors.white
              :
              Colors.black
            :
            color,
          fontFamily: fontFamily ?? "Baloo"
        ),
      )
    );
  }
}
